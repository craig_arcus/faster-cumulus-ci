#!/bin/bash

BASEDIR=`pwd`

# Checkout the CumulusCI repository
if [ -d ../faster-cumulus-ci ]; then
    cd ../faster-cumulus-ci
    # Uncomment and edit if you need a specific branch of CumulusCI
    #git fetch --all
    #git checkout feature/13-codeship-integration
    git pull
else
    git clone https://craig_arcus@bitbucket.org/craig_arcus/faster-cumulus-ci.git ../faster-cumulus-ci
    cd ../faster-cumulus-ci
    # Uncomment and edit if you need a specific branch of CumulusCI
    #git fetch --all
    #git checkout feature/13-codeship-integration
fi

# Run the codeship.sh from CumulusCI from BASEDIR
cd $BASEDIR
export CUMULUSCI_PATH=../faster-cumulus-ci
bash $CUMULUSCI_PATH/ci/codeship.sh

# Exit based on exit status of CumulusCI's codeship.sh
if [ $? != 0 ]; then exit 1; fi

