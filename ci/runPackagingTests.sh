export CUMULUSCI_PATH='faster-cumulus-ci'

function runAntTarget {
    target=$1
    ant $target  | stdbuf -oL \
        stdbuf -o L grep -v '^  *\[copy\]' | \
        stdbuf -o L grep -v '^  *\[delete\]' | \
        stdbuf -o L grep -v '^  *\[loadfile\]' | \
        stdbuf -o L grep -v '^  *\[mkdir\]' | \
        stdbuf -o L grep -v '^  *\[move\]' | \
        stdbuf -o L grep -v '^  *\[xslt\]'

    exit_status=${PIPESTATUS[0]}

    if [ "$exit_status" != "0" ]; then
        echo "BUILD FAILED on target $target"
    fi
    return $exit_status
}

export SF_USERNAME=$SF_USERNAME_PACKAGING
export SF_PASSWORD=$SF_PASSWORD_PACKAGING
export SF_SERVERURL=$SF_SERVERURL_PACKAGING

runAntTarget runAllTests
