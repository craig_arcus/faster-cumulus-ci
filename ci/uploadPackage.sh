# Upload beta package
echo
echo "-----------------------------------------------------------------"
echo "Uploading beta managed package via Selenium"
echo "-----------------------------------------------------------------"
echo

echo "Installing python dependencies"
export PACKAGE=`grep 'cumulusci.package.name.managed=' cumulusci.properties | sed -e 's/cumulusci.package.name.managed *= *//g'`
# Default to cumulusci.package.name if cumulusci.package.name.managed is not defined
if [ "$PACKAGE" == "" ]; then
    export PACKAGE=`grep 'cumulusci.package.name=' cumulusci.properties | sed -e 's/cumulusci.package.name *= *//g'`
fi
echo "Using package $PACKAGE"
export BUILD_NAME="$PACKAGE Build $CI_BUILD_NUMBER"
export BUILD_WORKSPACE=`pwd`
export BUILD_COMMIT="$CI_COMMIT_ID"
#pip install --upgrade selenium
# pip install selenium==2.44.0
# pip install --upgrade requests

echo
echo
echo "Running package_upload.py"
echo
python faster-cumulus-ci/ci/package_upload.py
if [[ $? -ne 0 ]]; then exit 1; fi

if [ "$GITHUB_USERNAME" != "" ]; then
    # Create GitHub Release
    echo
    echo "-----------------------------------------------------------------"
    echo "Creating GitHub Release $PACKAGE_VERSION"
    echo "-----------------------------------------------------------------"
    echo
    python faster-cumulus-ci/ci/github/create_release.py

    # Add release notes
    echo
    echo "-----------------------------------------------------------------"
    echo "Generating Release Notes for $PACKAGE_VERSION"
    echo "-----------------------------------------------------------------"
    echo
    pip install --upgrade PyGithub==1.25.1
    export CURRENT_REL_TAG=`grep CURRENT_REL_TAG release.properties | sed -e 's/CURRENT_REL_TAG=//g'`
    echo "Generating release notes for tag $CURRENT_REL_TAG"
    python faster-cumulus-ci/ci/github/release_notes.py


    # Merge master commit to all open feature branches
    echo
    echo "-----------------------------------------------------------------"
    echo "Merge commit to all open feature branches"
    echo "-----------------------------------------------------------------"
    echo
    python faster-cumulus-ci/ci/github/merge_master_to_feature.py
else
    echo
    echo "-----------------------------------------------------------------"
    echo "Skipping GitHub Releaseand master to feature merge because the"
    echo "environment variable GITHUB_USERNAME is not configured."
    echo "-----------------------------------------------------------------"
    echo
fi

# If environment variables are configured for mrbelvedere, publish the beta
if [ "$MRBELVEDERE_BASE_URL" != "" ]; then
    echo
    echo "-----------------------------------------------------------------"
    echo "Publishing $PACKAGE_VERSION to mrbelvedere installer"
    echo "-----------------------------------------------------------------"
    echo
    export NAMESPACE=`grep 'cumulusci.package.namespace *=' cumulusci.properties | sed -e 's/cumulusci\.package\.namespace *= *//g'`
    export PROPERTIES_PATH='version.properties'
    export BETA='true'
    echo "Checking out $CURRENT_REL_TAG"
    git fetch --tags origin
    git checkout $CURRENT_REL_TAG
    python faster-cumulus-ci/ci/mrbelvedere_update_dependencies.py
fi
