# NOTE: This script expects environment variables to be passed for the Salesforce
# credentials to the orgs: feature, master, packaging, beta, release such as...
# SF_USERNAME_FEATURE
# SF_PASSWORD_MASTER
# SF_SERVERURL_PACKAGING

export CUMULUSCI_PATH='faster-cumulus-ci'

# Setup variables for branch naming conventions using env overrides if set
if [ "$MASTER_BRANCH" == "" ]; then
    export MASTER_BRANCH='master'
fi
if [ "$PREFIX_FEATURE" == "" ]; then
    export PREFIX_FEATURE='feature/'
fi
if [ "$PREFIX_BETA" == "" ]; then
    export PREFIX_BETA='beta/'
fi
if [ "$PREFIX_RELEASE" == "" ]; then
    export PREFIX_RELEASE='release/'
fi

export APEX_TEST_NAME_MATCH_CUMULUSCI=`grep 'cumulusci.test.namematch *=' cumulusci.properties | sed -e 's/cumulusci.test.namematch *= *//g'`
export APEX_TEST_NAME_EXCLUDE_CUMULUSCI=`grep 'cumulusci.test.nameexclude *=' cumulusci.properties | sed -e 's/cumulusci.test.nameexclude *= *//g'`

# Get the PACKAGE_AVAILABILE_RETRY_COUNT from env or use default
if [ "$PACKAGE_AVAILABLE_RETRY_COUNT" == "" ]; then
    export PACKAGE_AVAILABLE_RETRY_COUNT=5
fi

# Get the PACKAGE_AVAILABILE_DELAY from env or use default
if [ "$PACKAGE_AVAILABLE_DELAY" == "" ]; then
    export PACKAGE_AVAILABLE_DELAY=0
fi

# The python scripts expect BUILD_COMMIT
export BUILD_COMMIT=$CI_COMMIT_ID

# Cache the main build directory
export BUILD_WORKSPACE=`pwd`

echo
echo "-----------------------------------------------------------------"
echo "Building $CI_BRANCH as a $BUILD_TYPE build"
echo "-----------------------------------------------------------------"
echo

# Function to filter out unneeded ant output from builds
function runAntTarget {
    target=$1
    ant $target  | stdbuf -oL \
        stdbuf -o L grep -v '^  *\[copy\]' | \
        stdbuf -o L grep -v '^  *\[delete\]' | \
        stdbuf -o L grep -v '^  *\[loadfile\]' | \
        stdbuf -o L grep -v '^  *\[mkdir\]' | \
        stdbuf -o L grep -v '^  *\[move\]' | \
        stdbuf -o L grep -v '^  *\[xslt\]'

    exit_status=${PIPESTATUS[0]}

    if [ "$exit_status" != "0" ]; then
        echo "BUILD FAILED on target $target"
    fi
    return $exit_status
}

function runAntTargetBackground {
    ant $1 > "$1.cumulusci.log" 2>& 1 &
}

# Function to wait on all background jobs to complete and return exit status
function waitOnBackgroundJobs {
    FAIL=0
    for job in `jobs -p`
    do
    echo $job
        wait $job || let "FAIL+=1"
    done

    echo
    echo "-----------------------------------------------------------------"
    if [ $FAIL -gt 0 ]; then
        echo "BUILD FAILED: Showing logs from parallel jobs below"
    else
        echo "BUILD PASSED: Showing logs from parallel jobs below"
    fi
    echo "-----------------------------------------------------------------"
    echo
    for file in *.cumulusci.log; do
        echo
        echo "-----------------------------------------------------------------"
        echo "BUILD LOG: $file"
        echo "-----------------------------------------------------------------"
        echo
        cat $file
    done
    if [ $FAIL -gt 0 ]; then
        exit 1
    fi
}

#-----------------------------------
# Set up dependencies for async test
#-----------------------------------
if [ "$TEST_MODE" == 'parallel' ]; then
    pip install --upgrade simple-salesforce
fi

#---------------------------------
# Run the build for the build type
#---------------------------------

# Master branch commit, build and test a beta managed package
# Set the APEX_TEST_NAME_* environment variables for the build type
if [ "$APEX_TEST_NAME_MATCH_MASTER" != "" ]; then
    export APEX_TEST_NAME_MATCH=$APEX_TEST_NAME_MATCH_MASTER
elif [ "$APEX_TEST_NAME_MATCH_GLOBAL" != "" ]; then
    export APEX_TEST_NAME_MATCH=$APEX_TEST_NAME_MATCH_GLOBAL
else
    export APEX_TEST_NAME_MATCH=$APEX_TEST_NAME_MATCH_CUMULUSCI
fi
if [ "$APEX_TEST_NAME_EXCLUDE_MASTER" != "" ]; then
    export APEX_TEST_NAME_EXCLUDE=$APEX_TEST_NAME_EXCLUDE_MASTER
elif [ "$APEX_TEST_NAME_EXCLUDE_GLOBAL" != "" ]; then
    export APEX_TEST_NAME_EXCLUDE=$APEX_TEST_NAME_EXCLUDE_GLOBAL
else
    export APEX_TEST_NAME_EXCLUDE=$APEX_TEST_NAME_EXCLUDE_CUMULUSCI
fi

# Get org credentials from env
export SF_USERNAME=$SF_USERNAME_PACKAGING
export SF_PASSWORD=$SF_PASSWORD_PACKAGING
export SF_SERVERURL=$SF_SERVERURL_PACKAGING
echo "Got org credentials for packaging org from env"

# Deploy to packaging org
echo
echo "-----------------------------------------------------------------"
echo "ant deployCIPackageOrg - Deploy to packaging org"
echo "-----------------------------------------------------------------"
echo

#echo "Running deployCIPackageOrg from /home/rof/clone"
#cd /home/rof/clone
runAntTarget deployCIPackageOrg
if [[ $? != 0 ]]; then exit 1; fi


#echo
#echo "-----------------------------------------------------------------"
#echo "Waiting on background jobs to complete"
#echo "-----------------------------------------------------------------"
#echo
#waitOnBackgroundJobs
#if [ $? != 0 ]; then exit 1; fi
